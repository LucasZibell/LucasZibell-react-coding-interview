import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message, DatePicker, Form } from 'antd';
import Modal from 'react-modal';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

import modalStyles from './modalStyles';

const PersonDetail = () => {
  const [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const closeModal = () => setIsOpen(false);

  const saveChanges = () =>
    useEffect(() => {
      load();
    }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={() => setIsOpen(true)}>
            Edit
          </Button>,
        ]}
      >
        {data && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>

            <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
          </Descriptions>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
      <Modal isOpen={isOpen} style={modalStyles} contentLabel="Example Modal">
        <Form>
          <Form.Item label="Name" name="">
            <Input style={{ marginBottom: '10px' }} />
          </Form.Item>
          <Form.Item label="Apellido">
            <Input style={{ marginBottom: '10px' }} />
          </Form.Item>
          <Form.Item label="Gender">
            <Input style={{ marginBottom: '10px' }} />
          </Form.Item>
          <Form.Item label="Phone">
            <Input style={{ marginBottom: '10px' }} />
          </Form.Item>
          <Form.Item label="Birthday">
            <DatePicker style={{ marginBottom: '10px' }} />
          </Form.Item>
          <Form.Item>
            <button type="submit" onClick={saveChanges}>
              Save
            </button>
          </Form.Item>
        </Form>
        <button type="button" onClick={closeModal}>
          Close Modal
        </button>
      </Modal>
    </>
  );
};

export default withContextInitialized(PersonDetail);
